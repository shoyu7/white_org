Rails.application.routes.draw do
  get 'missions/new'
  get 'missions/show'
  get 'missions/update'
  get 'missions/delete'
  
  get 'static_pages/home'
  get '/about',    to: 'static_pages#about'
  get '/privacy',  to: 'static_pages#privacy'
  get '/kiyaku',   to: 'static_pages#kiyaku'
  
  get '/auth/:provider/callback', to: 'sessions#create'
  get '/logout', to: 'sessions#destroy'

  root 'static_pages#home'
  
  resources :missions
end
