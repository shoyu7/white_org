class SessionsController < ApplicationController
  def create
    user = User.find_or_create_from_auth(request.env['omniauth.auth'])
    if user
      log_in user
      remember user
      flash[:notice] = "ユーザー認証が完了しました。"
      redirect_to root_path
    else
      # エラーメッセージを作成する
      flash.now[:danger] = 'Invalid email/password combination'
      redirect_to root_path
    end
  end

  def destroy
    log_out if logged_in?
    flash[:notice] = "ログアウトしました。"
    redirect_to root_path
  end
end
