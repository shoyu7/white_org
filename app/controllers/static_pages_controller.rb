class StaticPagesController < ApplicationController
  def home
    if logged_in?
      redirect_to missions_path
    end
  end

  def about
  end
  
  def privacy
  end
  
  def kiyaku
  end
  
  
end
