class MissionsController < ApplicationController
  before_action :logged_in_user, only: [:index, :new, :update, :delete]
  
  def index
    @missions = Mission.all
  end
  
  def new
    @mission = Mission.new
  end

  def show
    @mission = Mission.find(params[:id])
  end

  def update
  end

  def delete
  end
  
  
  #### Private ####
  private
  
  
    # ログイン済みユーザーかどうか確認
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to root
      end
    end
end
