# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Mission.create(name:"敵を倒せ！", desc:"<b>厄介な相手だよ</b>", user_id: 1)
Mission.create(name:"相手の足をひっかけろ！", desc:"<b>厄介な相手だよ</b>", user_id: 1)
Mission.create(name:"図書館に向かえ！", desc:'<div class="balloon5">
  <div class="faceicon">
   <img src="/oldman_75.png">
  </div>
  <div class="chatting">
    <div class="says">
      <p>月日は百代の過客にして雪こう年も又旅人なりアイウエオかきくけこさしすせそたちつてとなにぬねのかきくけこサッカー部の部員数は6000人以上になります。</p>
      <p>少しでもラグビーの楽しさをわかってもらおうと長い人生を通してラグビーを愛せるように</p>
    </div>
  </div>
</div>', user_id: 1)
Mission.create(name:"シンクピカピカ大作戦", desc:"<b>シンクがピッカピカになってしまったよ！</b>", user_id: 1)